-- SUMMARY --

Allow the administrator to manage federated tables and associate it with Drupal tables and Views.

-- REQUIRENENTS --


-- INSTALLATION --


-- CONFIGURATION --


-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Yvan Marques (yvmarques) - http://drupal.org/user/298685